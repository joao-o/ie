set terminal  png
set output "graf.png"
set samples 1000
f(x)=a+b*exp(-c*x)*sin(d*x+e)
a=10200.0
b=10000.0
c=0.0071
d=0.1
e=1.57
fit f(x) "dados.txt" every 1::1::500 using 1:2 via a,b,c,d,e
plot f(x), "dados.txt" every 1::1::500 u 1:2
pause -1
