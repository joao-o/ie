set terminal  png
set output "graf.png"
set samples 1000
set style line 2 lc rgb '#FF0000' lt 1 lw 1 pt 2 ps 0.2

f(x)=a+b*exp(-c*x)*sin(d*x*2*pi+e)
a=300.0
b=147.0
c=0.5
d=1
e=-1

fit f(x) "dados" every 1::90::300 using ($1*0.015+$2*0.0465/343000):($2*0.0465):(1*0.0465) yerrors via a,b,c,d,e
plot "dados" every 1::90::300 u ($1*0.015+$2*0.0465/343000):($2*0.0465):(1*0.0465) with yerrorbars ls 2 notitle,\
f(x) notitle
pause -1
