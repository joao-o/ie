set terminal png 
set output "reta.png"
set samples 1000
set style line 2 lc rgb '#FF0000' lt 1 lw 1 pt 2 ps 0.2

f(x)=a*x+b
a=300.0
b=150.0
set ylabel "t(ms)"
set xlabel "2*d(mm)"
c=2.712967986977753e-4

fit f(x) "retaout.dat" using ($1*20):($2*c):($3*c) yerrors via a,b
plot "retaout.dat" u ($1*20):($2*c):($3*c) with yerrorbars ls 2 notitle,\
f(x) notitle
pause -1
