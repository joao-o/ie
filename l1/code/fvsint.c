#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#define magic 465.5453065653825 

int 
main(int argc,char **argv)
{
   uint16_t resint,i;
	double resfloat; 
	FILE *file;
	file=fopen("out.dat","w");

	for(i=65535;i!=0;i--){
		fprintf(file,"%u ",i);
		resint = i*i;
		fprintf(file,"%u ",resint);
		resfloat = (double)i*(0.04655453065653824);
		fprintf(file,"%f ",resfloat);

		resint = (i/1000)*466+((i%1000)*466)/1000;
		fprintf(file,"%u ",resint);
		resint = (i/1000);
		fprintf(file,"%u ",resint);
		resint = (i/1000)*466;
		fprintf(file,"%u ",resint);
		resint = (i%1000);
		fprintf(file,"%u ",resint);
		resint = (i%1000)*466;
		fprintf(file,"%u ",resint);
		resint = ((i%1000)*466)/1000;
		fprintf(file,"%u ",resint);

		fprintf(file,"%f ",(((double)resint)-resfloat*10)/(resfloat*10)*100);
		resint = (i/21);
		fprintf(file,"%u ",resint);
		fprintf(file,"%f ",(((double)resint)-resfloat)/resfloat*100);
		putc('\n',file);
	}

	return 0;
}
