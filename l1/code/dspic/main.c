#include <libpic30.h>			  //C30 compiler definitions
#include <uart.h>					  //UART (serial port) function and utilities library
#include <timer.h>				  //timer library
#include <stdio.h>
#include <stdlib.h>
#include <outcompare.h>
#include <adc10.h>
#include <incap.h>

#define FCY ((long) 7372*4)	  //instruction frequency in kHz

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF);					  //watchdog timer is off

#define RXBSZ 20

unsigned int str_pos = 0;
char rx_buf[RXBSZ];				  //buffer used to store characters from serial port
volatile unsigned int valor, ic_ev, command_avail;	/* flags changed in interrupts */
unsigned int n_data, print;
unsigned int data[600];

unsigned int t2_int_ct = 0;
unsigned int pulses = 10;

//Reads UART commands
void
intrep_command(void)
{
	unsigned int tmp;
	rx_buf[str_pos - 1] = 0;

	switch (rx_buf[0]) {
	case 't':						  // Sets time between each pulse train
		PR4 = ((unsigned int) atoi(rx_buf + 1)) * 96 / 5 * 6;
		break;
	case 'p':						  // sets lenght of pulse train
		pulses = atoi(rx_buf + 1) - 1;
		printf("%d\n\r", pulses);
		break;
	case 'a':						  // configure and start aquisition mode
		n_data = atoi(rx_buf + 1);
		if (n_data > 600) n_data = 600;
		printf("Contagem Iniciada!\n\r");
		break;
	case 'v':						  // Toggle quiet mode
		print ^= 1;
		break;
	case 'f':						  // Change frequency
		tmp = atoi(rx_buf + 1);
		PR2 = OC1RS = tmp;
		OC3R = OC3RS = OC1R = tmp/2;
		break;
	default:
		puts("?\r");
	}
	str_pos = 0;
	command_avail = 0;
}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{
	IFS1bits.U2RXIF = 0;
	char c;
	while (U2STAbits.URXDA) {	  // reads bytes from the uart into the buffer
		c = U2RXREG;
		putchar(c);
		rx_buf[str_pos++] = c;
		if (str_pos >= RXBSZ) {	  // rx_buf is a circular buffer
			str_pos = 0;
		}
	}
	if (c == '\r') {				  // check for a newline in buffer
		command_avail = 1;
		putchar('\n');
		putchar('\r');
	}
}

/* timer 2 is used for output compare
 * here we send a finite amount of pulses
 * and stop until the next cycle
 */
void __attribute__ ((interrupt, auto_psv)) _T2Interrupt(void)
{
	IFS0bits.T2IF = 0;
	if (t2_int_ct++ == pulses) { // Stops creating pulses when desired number is achieved
		T2CONbits.TON = 0;
		t2_int_ct = 0;
	}
}

/*timer 4 is used to send pulse trains periodicly*/
void __attribute__ ((interrupt, auto_psv)) _T4Interrupt(void)
{
	IFS1bits.T4IF = 0;
	_LATF0 = 1;

	TMR2 = 0;						  /* first reset  timers */
	TMR3 = 0;
	T3CONbits.TON = 1;			  /* enable them (almost) simultaneously */
	T2CONbits.TON = 1;			  /* this will begin pulse generation */
	_IC8IE = 1;						  /* activate the IC interrupt flags and the IC itself */
	IC8CONbits.ICM = 2;			  /* it was off to screen off unwanted higer order echoes */
}

/* input capture is running on timer 3 
 * when a capture event has been triggered we have recieved an echo 
 * so here we store it and tell main() about it 
 * we also disable both the timer and the input capture
 * to avoid reception of more echoes and of 
 * capture events from the same pulse train 
 * which would flood the IC buffer
 */
void __attribute__ ((interrupt, auto_psv)) _IC8Interrupt(void)
{
	IFS1bits.IC8IF = 0;
	T3CONbits.TON = 0;
	TMR3 = 0;
	_LATF0 = 0;
	valor = IC8BUF;
	ic_ev = 1;
	IC8CONbits.ICM = 0;
}

int
main(void)
{
	unsigned int config0, config1;
	unsigned int i = 0;

	_LATF0 = 1;						  //LED cfg
	_TRISF0 = 0;
	_TRISD0 = 0;					  // oc1
	_TRISB5 = 1;					  // ic7
	_LATB5 = 1;
	_TRISD2 = 0;					  // oc3

	ADPCFG = 0xFFFF;				  /* IC8 is used so we need to ser portB pins as digital */

	/* Serial port config */
	//activates the uart in continuous mode (no sleep) and 8bit no parity mode7372*4
	config0 = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
	//activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
	config1 = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
	OpenUART2(config0, config1, 15);	// umode, u2sta, 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1;					  //0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0;		  //disables hardware loopback on UART2. Enable only for tests
	U2MODEbits.STSEL = 0;
	__C30_UART = 2;				  //define UART2 as predefined for use with stdio library, printf etc
	puts("\n\rSerial port ONLINE\r");	//to check if the serial port is working

	/*pwm configuration */
	config0 = OC_TIMER2_SRC & OC_PWM_FAULT_PIN_DISABLE;
	OpenOC3(config0, 369, 369);  // ocxcon, ocxrs, ocxr
	config0 = OC_TIMER2_SRC & OC_CONTINUE_PULSE;
	OpenOC1(config0, 727, 369);
	config0 = T2_OFF & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT;
	OpenTimer2(config0, 727);	  // tcon
	_T2IE = 1;

	config0 = T4_ON & T4_GATE_OFF & T4_PS_1_256 & T4_SOURCE_INT;
	OpenTimer4(config0, 65535);  // tcon
	_T4IE = 1;

	config0 = IC_TIMER3_SRC & IC_INT_1CAPTURE & IC_EVERY_FALL_EDGE;
	IC8CON = config0;				  //icxcon
	config0 = T3_OFF & T3_GATE_OFF & T3_PS_1_8 & T3_SOURCE_INT;
	OpenTimer3(config0, 65535);
	IEC1bits.IC8IE = 1;

	/* main execution cycle */

	while (1) {
		if (command_avail)
			intrep_command();
		if (ic_ev && str_pos == 0) {	/* only print things if the user is not writing */
			if (n_data) {			  // Aquiring mode
				data[i++] = valor;

				if (i == n_data) {  // After getting all values print them all
					puts("Contagem Terminada!\r");
					for (i = 0; i < n_data; i++)
						printf("%u \t %u\n\r", i, data[i]);
					puts("-------\r");
					n_data = 0;
					i = 0;
				}
			} else {
				if (print)			  // If quiet mode is off prints current value
					printf("%u\t%u\r\n", valor / 217 * 10 + (valor % 217) * 10 / 217,
							 valor);
				/* rough aproximation of distance in milimeters */
			}
			ic_ev = 0;
		}
	}
}
