set terminal png
set xlabel "s"
set ylabel "V"
set grid
unset logscale x 
set xrange [0.000000e+00:1.500000e-03]
unset logscale y 
set yrange [2.5-1.901249e-03:2.5+1.928170e-03]
#set xtics 1
#set x2tics 1
#set ytics 1
#set y2tics 1
set format y "%g"
set format x "%g"
plot 'IAMPTIME.data' using 1:($2+2.5) with lines lw 1 title "v(8)" 
