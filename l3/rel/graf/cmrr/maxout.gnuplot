set terminal png  size 600, 500
set output "../cmrr.png"
set xrange [0.01:300.0]
set yrange [0.0:66.6215629113507]
set xlabel "f(Hz)"
set ylabel "CMRR(dB)"
set grid xtics ytics mxtics
set mxtics 10
set logscale x
plot './data.gnuplot' index 0 t '' w l lw 1 lt 1 lc rgb '#0000ff' axis x1y1
