set terminal png 
set output "../filt.png"
set xrange [1.0:2300.0]
set yrange [-0.2129748824904284:0.21304092835342678]
set grid xtics ytics
set title ''
set xlabel 't(ms)'
set ylabel 'U(V)'
plot '/home/joao/data.gnuplot' index 0 t 'I' w p ps 1 pt 1 lc rgb '#0000ff' axis x1y1, \
'/home/joao/data.gnuplot' index 1 t 'II' w p ps 1 pt 1 lc rgb '#ff0000' axis x1y1, \
'/home/joao/data.gnuplot' index 2 t 'III' w p ps 1 pt 1 lc rgb '#ffa500' axis x1y1
