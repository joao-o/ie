set terminal gif enhanced medium animate delay 4
set output "anim.gif"

set xrange [-0.3:0.3]
set yrange [-0.3:0.5]
set xlabel "Ux(V)"
set ylabel "Uy(V)"
set grid xtics ytics

do for [i=1:799:1] {
  plot "out.txt" every 1::i::i u (0):(0):1:2 with vectors title sprintf("t=%i ms",i)
}
unset multiplot
