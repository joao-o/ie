set terminal gif enhanced medium animate delay 4
set output "anim2.gif"

set xrange [0:800]
set yrange [-1.3:1.5]
set grid xtics ytics
set xlabel "t(ms)"
set ylabel "U(V)"

do for [i=1:799:1] {
  set arrow 1 from i,-1.3 to i,1.5 nohead lc "#ff0000"
  plot "out2.txt" u 1:($2+1) title "aVr+1",\
       "out2.txt" u 1:($3)   title "aVl",\
       "out2.txt" u 1:($4-1) title "aVf-1"
 
}
unset multiplot
