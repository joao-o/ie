Instrumentação Electrónica  -  MEFT 2014/15

                     4º Trabalho Prático de Laboratório


Nome:									nº



Nome: 									nº





Grupo nº: 		Turno:					Data:



Objectivo



   1) Compreender a utilidade e a importância dos amplificadores de instrumentação e o método da medida diferencial

   2) Incorporar filtros para condicionar o sinal e eliminar o ruído pela limitação da banda passante;

   3) Maximizar a utilidade dos andares activos nos circuitos electrónicos.

   4) Aplicar a análise espectral à compreensão e interpretação de sinais.

Procedimento

Primo (L1)

      

   1. Projecte um amplificador de instrumentação de muito alta impedância de entrada com o objectivo de captar os sinais polares de estímulo do musculo cardíaco.

   2. Introduza uma filtragem de modo a eliminar qualquer componente DC e a rejeitar as componentes de alta frequência. Efectue a respectiva simulação SPICE.

   3. Aprove o circuito desenhado com o docente.

Secundo (L2)

   4. Monte o circuito na breadboard;

   5. Obtenha experimentalmente a resposta em frequência do circuito para uma gama de 1Hz a 200Hz.

   6. Meça a relação de rejeição do modo comum (CMR) do circuito.

   7. Adquira para um ficheiro as amostras do o electrocardiograma(ECG) com o osciloscópio, se possível para duas ou três "LEAD".

Terctio (L3)

   8. Analise o espectro do ECG com o Octave/Matlab ou outra aplicação numérica e especule sobre a decisão do filtro utilizado e os limites de aplicação do mesmo 

Descrição do Trabalho Realizado

(editar livremente o documento, inserindo gráficos ou tabelas dos resultados e, quando necessário, os códigos fontes comentados)

