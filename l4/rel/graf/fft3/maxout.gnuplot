set terminal png 
set output "../fft3.png"
set xrange [0.48828125:97.65625]
set xlabel 'f(Hz)'
set ylabel 'log10(abs(Cn))'
plot './data.gnuplot' index 0 using 1:(log10($2)) w p ps 1 pt 1 lc rgb '#0000ff' notitle
