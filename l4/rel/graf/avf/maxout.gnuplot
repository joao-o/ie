set terminal png 
set output "../avf.png"
set xrange [1.0:2000.0]
set yrange [-0.1:0.4]
set grid xtics ytics
set title ''
set xlabel 't(ms)'
set ylabel 'U(V)'
plot './data.gnuplot' index 0 t '' w p ps 1 pt 1 lc rgb '#0000ff' axis x1y1,
