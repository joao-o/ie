#include <libpic30.h>			  //C30 compiler definitions
#include <uart.h>					  //UART (serial port) function and utilities library
#include <timer.h>				  //timer library
#include <stdio.h>
#include <stdlib.h>
#include <outcompare.h>
#include <adc10.h>
#include <incap.h>

#define FCY ((long) 7372*4)	  //instruction frequency in kHz

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF);					  //watchdog timer is off

#define RXBSZ 20

unsigned int str_pos = 0;
char rx_buf[RXBSZ];				  //buffer used to store characters from serial port
volatile unsigned int valor, ic_ev, command_avail;	/* flags changed in interrupts */
unsigned int n_data, print;
unsigned int data[12];
unsigned int i=0;
unsigned int f=17500;
unsigned int j,invalid,t,v,a1,a2;
unsigned int acc=1;
unsigned int controlo=1;

unsigned int t2_int_ct = 0;
unsigned int pulses = 10;
unsigned int trig=0;

//Reads UART commands
void
intrep_command(void)
{
	unsigned int tmp;
	rx_buf[str_pos - 1] = 0;

	switch (rx_buf[0]) {
	case 't':						  // Sets time between each pulse train
	  T2CONbits.TON^=1;
	  controlo=!T2CONbits.TON;
	  break;
	case 'a':						  // configure and start aquisition mode
	  printf("buffer reset");
	  i=0;
	  IC8CONbits.ICM=3;
	  break;
	case 'a':						  // start
	  _LATB3=1;
	  _LATB4=0;
	  controlo=0;
	  break;
	case 's':						  // stop
	  _LATB3=0;
	  _LATB4=1;
	  controlo=0;
	  break;
	case 'r':                         //rpm input
	  f=atoi(rxbuf[1]);
	  if(f<10000 || f>18000){
	    puts("velocidade nao permitida!");
	    f=17000
	  }
	  else{
	    a1=f/10;
	    a2=29091/a1+29091%a1*100/a1;
	    f=a2*1000
	  }
	  controlo=1;
	  break;
	default:
	  puts("?\r");
	}
	str_pos = 0;
	command_avail = 0;
}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{
	IFS1bits.U2RXIF = 0;
	char c;
	while (U2STAbits.URXDA) {	  // reads bytes from the uart into the buffer
		c = U2RXREG;
		putchar(c);
		rx_buf[str_pos++] = c;
		if (str_pos >= RXBSZ) {	  // rx_buf is a circular buffer
			str_pos = 0;
		}
	}
	if (c == '\r') {				  // check for a newline in buffer
		command_avail = 1;
		putchar('\n');
		putchar('\r');
	}
}

/* input capture is running on timer 3 
 */
void __attribute__ ((interrupt, auto_psv)) _IC8Interrupt(void)
{
  IFS1bits.IC8IF = 0;
  if(IC8BUF<10000){
    data[i++]=IC8BUF;
    _LATF0 ^=1;
    PR3=0;
    if(i==5) {
      trig=1;
      i=0;
    }
  }
}

void
crtl(void){
    if(t>f){
        _LATB3=1;
        _LATB4=0;
    }
    else {
        _LATB4=1;
        _LATB3=0;
    }
}

int
main(void)
{
	unsigned int config0, config1;

	_LATF0 = 1;						  //LED cfg
	_TRISF0 = 0;
	_TRISD0 = 0;					  // oc1
	_TRISB5 = 1;					  // ic8
	_LATB5 = 0;
	_TRISD2 = 0;					  // oc3
	_TRISB3 = 0; //desacelera
	_LATB3 = 0; 
	_TRISB4 = 0; // acelera
	_LATB4 = 1;

	ADPCFG = 0xFFFF;				  /* IC8 is used so we need to ser portB pins as digital */

	/* Serial port config */
	//activates the uart in continuous mode (no sleep) and 8bit no parity mode7372*4
	config0 = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
	//activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
	config1 = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
	OpenUART2(config0, config1, 15);	// umode, u2sta, 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1;					  //0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0;		  //disables hardware loopback on UART2. Enable only for tests
	U2MODEbits.STSEL = 0;
	__C30_UART = 2;				  //define UART2 as predefined for use with stdio library, printf etc
	puts("\n\rSerial port ONLINE\r");	//to check if the serial port is working


	config0 = IC_TIMER3_SRC & IC_INT_1CAPTURE & IC_EVERY_RISE_EDGE;
	IC8CON = config0;				  //icxcon
	config0 = T3_ON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT;
	OpenTimer3(config0, 65535);
	IEC1bits.IC8IE = 1;

	/* main execution cycle */

	delay_ms(5000);
	
	while (1) {
		if (command_avail)
			intrep_command();
        if(trig==1){
            for(j=1;j<6;j++){
                if(data[0]<data[j]*0.7 || data[0]>data[j]*1.4){
                    invalid=1;
                    if(print)
                        puts("\n invalid measurement\r pick a value in [10000,18000]");
                    break;
                }
            }
            t=0;
            if(invalid==0){
                for(j=0;j<6;j++){t=t+data[j]/6;}
                if(print)
                    puts("\n%d\r",v);
		if(controlo)
		  crtl();
            }
        }
	}
    
    return 0;
}

* 1ms delay function */
void delay_ms(unsigned int delay) //delay in miliseconds
{
 	unsigned int cycles;	// number of cycles
 	for(;delay;delay--)
    for(cycles=FCY/4;cycles;cycles--); //~1ms cycle
}

